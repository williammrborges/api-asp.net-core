using APICalculoJuros.Requests;
using APICalculoJuros.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace CalculaJurosTest
{
    public class CalculaJurosTest
    {

        public HttpClient Client { get; set; }
        private readonly TestServer _server;
        private readonly CalculoJurosService _calculaJuros = new CalculoJurosService();

        public CalculaJurosTest()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Desenvolvimento")
                .UseStartup<API.Startup>());
            Client = _server.CreateClient();
        }

        [Fact]
        public async System.Threading.Tasks.Task ValidaCalculoJurosIntegracaoAsync()
        {
            decimal valorInicial = 100;
            int meses = 5;

            var response = await Client.GetAsync("/CalculoJuros/CalculaJuros").ConfigureAwait(true);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var resultado = response.Content.ReadAsStringAsync().Result;

            Assert.Equal("0", resultado);

            response = await Client.GetAsync($"/CalculoJuros/CalculaJuros?valorInicial={valorInicial}&meses={meses}")
                .ConfigureAwait(true);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            resultado = response.Content.ReadAsStringAsync().Result;

            Assert.Equal("105.10", resultado);

            valorInicial = 0;
            meses = 5;
            response = await Client.GetAsync($"/CalculoJuros/CalculaJuros?valorInicial={valorInicial}&meses={meses}")
                .ConfigureAwait(true);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            resultado = response.Content.ReadAsStringAsync().Result;

            Assert.Equal("0", resultado);

            valorInicial = 100;
            meses = 0;
            response = await Client.GetAsync($"/CalculoJuros/CalculaJuros?valorInicial={valorInicial}&meses={meses}")
                .ConfigureAwait(true);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            resultado = response.Content.ReadAsStringAsync().Result;

            Assert.Equal("100", resultado);
        }

        [Fact]
        public void RetornaValorFinalJuros()
        {

            CalculaJurosRequest request = new CalculaJurosRequest
            {
                Meses = 5,
                ValorInical = 100
            };
            var juros = _calculaJuros.RetornaValorFinal(request);
            Assert.Equal(new decimal(105.10), Math.Round(juros, 2, MidpointRounding.AwayFromZero));

            request = new CalculaJurosRequest
            {
                Meses = 0,
                ValorInical = 100
            };
            juros = _calculaJuros.RetornaValorFinal(request);
            Assert.Equal(new decimal(100), Math.Round(juros, 2, MidpointRounding.AwayFromZero));

            request = new CalculaJurosRequest
            {
                Meses = 0,
                ValorInical = 0
            };
            juros = _calculaJuros.RetornaValorFinal(request);
            Assert.Equal(new decimal(0), Math.Round(juros, 2, MidpointRounding.AwayFromZero));

            request = new CalculaJurosRequest
            {
                Meses = 5,
                ValorInical = 0
            };
            juros = _calculaJuros.RetornaValorFinal(request);
            Assert.Equal(new decimal(0), Math.Round(juros, 2, MidpointRounding.AwayFromZero));
        }

        [Fact]
        public async System.Threading.Tasks.Task ValidaShowMeTheCodeIntegracaoAsync()
        {
            var response = await Client.GetAsync("/CalculoJuros/ShowMeTheCode").ConfigureAwait(true);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var resultado = response.Content.ReadAsStringAsync().Result;

            Assert.Equal("\"https://gitlab.com/williammrborges/api-asp.net-core\"", resultado);
        }
    }
}
