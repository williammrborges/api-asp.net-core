using APITaxaJuros.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace TaxaJurosTeste
{
    public class TaxaJurosTeste
    {
        public HttpClient Client { get; set; }
        private readonly TestServer _server;
        private readonly TaxaJurosService _taxaJuros = new TaxaJurosService();

        public TaxaJurosTeste()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Desenvolvimento")
                .UseStartup<API.Startup>());
            Client = _server.CreateClient();
        }

        [Fact]
        public void ValidaTaxa()
        {
            //Valida o valor de retorno da taxa de juros.
           var taxaJuros = _taxaJuros.RetornaValorTaxaJuros();
           Assert.Equal(0.01, taxaJuros);
        }

        [Fact]
        public async System.Threading.Tasks.Task ValidaTaxaIntegracaoAsync()
        {
            //Verifica se o servi�o est� respondendo corretamente.
            HttpResponseMessage response = await Client.GetAsync("/TaxaJuros/RetornaTaxaJuros").ConfigureAwait(true);
            response.EnsureSuccessStatusCode();            
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var taxaJuros = double.Parse(response.Content.ReadAsStringAsync().Result, System.Globalization.CultureInfo.InvariantCulture);
            Assert.Equal(0.01, taxaJuros);
        }
    }
}
