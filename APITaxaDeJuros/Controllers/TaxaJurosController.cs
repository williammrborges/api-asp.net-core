﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APITaxaJuros.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TaxaJurosController : ControllerBase
    {
        private readonly TaxaJurosService _service = new TaxaJurosService();

        //Retorna o valor da taxa de juros.
        [HttpGet]
        [Route("RetornaTaxaJuros")]
        public Double RetornaTaxaDeJuros() => _service.RetornaValorTaxaJuros();
    }
}
