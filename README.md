# Api ASP.Net Core

Comunicação entre API's Asp.Net CORE

Desenvolvido em C#, utilizado Swagger para documentação de API e Docker para utilização

Para executar o Docker (utilizado máquina com linux mint)

IP Da VM: 192.168.0.17
Alterar a chamada da API na API de cálculo de juros de acordo com o ip da máquina utilizada.

url:http://192.168.0.17:8080/

Na pasta da API calcula juros
- sudo docker build -t calculojuros -f Dockerfile .

Na pasta da API taxa juros
- sudo docker build -t taxajuros -f Dockerfile .

- sudo docker images

- sudo docker create taxajuros
- sudo docker create calculojuros

- sudo docker ps -a

docker run -d -p 8080:80 taxajuros
docker run -d -p 8081:80 calculojuros
docker stop taxajuros

