﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using APICalculoJuros.Requests;
using APICalculoJuros.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    [Route("[controller]")]
    public class CalculoJurosController : ControllerBase
    {

        private readonly CalculoJurosService _service = new CalculoJurosService();
        
        [HttpGet]
        [Route("CalculaJuros")]
        [Consumes("application/json")]
        public string CalculaJuros([FromQuery]decimal valorInicial, [FromQuery]int meses)
        {
            if (valorInicial < 0 || meses < 0)
            {
                return "0";
            }

            //Valor inicial é um decimal recebido como parâmetro.
            //Tempo é um inteiro, que representa meses, também recebido como parâmetro.
            CalculaJurosRequest request = new CalculaJurosRequest { Meses = meses, ValorInical = valorInicial };

            //Resultado final deve ser truncado (sem arredondamento) em duas casas decimais.
            //Juros Compostos: Valor Final = Valor Inicial *(1 + Taxa de Juros) ^ Tempo
            var valorFinal = _service.RetornaValorFinal(request);

            //Retorna o valor sem arredondamento com 2 casas fixas.
            return valorFinal;
        }

        [HttpGet]
        [Route("ShowMeTheCode")]
        public String ShowMeTheCode()
        {
            //Retorna a url onde está o projeto
            return "https://gitlab.com/williammrborges/api-asp.net-core";
        }
    }
}
