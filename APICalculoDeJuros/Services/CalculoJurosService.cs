﻿using APICalculoJuros.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace APICalculoJuros.Services
{
    public class CalculoJurosService
    {
        public string RetornaValorFinal(CalculaJurosRequest request)
        {
            //Valor do Juros deve ser consultado na API Taxa de Juros.
            var valorJuros = new Double();
            try
            {
                using (var client = new HttpClient())
                {
                    //Realiza a consulta na API de taxa de juros
                    client.DefaultRequestHeaders.Accept.Clear();

                    string baseURL = "http://192.168.0.17:8080/TaxaJuros/RetornaTaxaJuros";
                    HttpResponseMessage response = client.GetAsync(baseURL).Result;

                    valorJuros = double.Parse(response.Content.ReadAsStringAsync().Result, System.Globalization.CultureInfo.InvariantCulture);

                }
            }
            catch
            {
                return "Erro de conexão com a API";
            }

            var valorFinal = Math.Round((request.ValorInical * new decimal(Math.Pow((1 + valorJuros), request.Meses))), 2, MidpointRounding.AwayFromZero);
            //Realiza o cálculo de juros composto.
            return valorFinal.ToString();
        }
    }
}
