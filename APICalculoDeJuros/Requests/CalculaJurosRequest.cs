﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APICalculoJuros.Requests
{
    //Classe de recebimento de valores para o cálculo do juros composto.
    public class CalculaJurosRequest
    {
        public decimal ValorInical { get; set; }
        public int Meses { get; set; }
    }
}
